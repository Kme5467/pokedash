package PokeData;

import java.util.ArrayList;

/**
 * Created by kelseyedge on 2/8/17.
 */
public class Trainer {
    private String age;
    private String name;
    private String gender;
    private ArrayList<Pokemon> pokemonList = new ArrayList<>();


    public Trainer(String age, String name, String gender) {
        this.age = age;
        this.name = name;
        this.gender = gender;
    }

    /**
     * Add list of Trainers Pokemon as Pokemon objects
     * @param name
     * @param pokedexId
     * @param typeA
     * @param typeB
     * @param level
     */
    public void addPokemon(String name, String pokedexId, String typeA, String typeB, String level){
        Pokemon x = new Pokemon(typeA, level, name, pokedexId, typeB);
        pokemonList.add(x);
    }

    /**
     * Given a pokedexId, should locate a pokemon in trainers pokemonList. If the pokemon is not there,
     * return null
     * @param pokedexId
     * @return Pokemon object
     */
    public Pokemon locatePokemon(String pokedexId){
        for (Pokemon aPokemon : pokemonList)
            if (aPokemon.getPokedexId().equals(pokedexId)){
            return aPokemon;
            }
        return null;
    }

    /**
     * Prints list of pokemon the belong to a trainer
     */
    public void listPokemon() {
        for (Pokemon apokemonList : pokemonList){
            System.out.println(apokemonList);
        }
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Trainer [pokemon : " + ", age : " + age + ", name : " + name + ", gender : " + gender + "]";
    }
}
