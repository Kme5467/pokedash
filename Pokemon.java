package PokeData;

/**
 * Created by kelseyedge on 2/8/17.
 */
public class Pokemon {
    private String typeA;
    private String level;
    private String name;
    private String pokedexId;
    private String typeB;

    public Pokemon(String typeA, String level, String name, String pokedexId, String typeB) {
        this.typeA = typeA;
        this.level = level;
        this.name = name;
        this.pokedexId = pokedexId;
        this.typeB = typeB;
    }

    public String getTypeA() {
        return typeA;
    }

    public void setTypeA(String typeA) {
        this.typeA = typeA;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPokedexId() {
        return pokedexId;
    }

    public void setPokedexId(String pokedexId) {
        this.pokedexId = pokedexId;
    }

    public String getTypeB() {
        return typeB;
    }

    public void setTypeB(String typeB) {
        this.typeB = typeB;
    }

    @Override
    public String toString() {
        return "Pokemon [pokedexId : " + pokedexId + ", name : " + name + ", level : " + level + ", typeA : " + typeA + ", typeB : " + typeB + "]";
    }
}
